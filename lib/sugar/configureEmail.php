<?php
define('sugarEntry', true);

require_once('include/entryPoint.php');

$MAILTRAP_HOST = getenv('MAILTRAP_HOST');
$MAILTRAP_PORT = getenv('MAILTRAP_PORT');

global $current_user, $db;
$current_user = new User();
$current_user->getSystemUser();

$om = new OutboundEmail();
$om->getSystemMailerSettings();

$om->mail_smtpserver = $MAILTRAP_HOST;
$om->mail_smtpport = $MAILTRAP_PORT;
$om->mail_smtpauth_req = 0;
$om->mail_smtpssl = 0;

$om->saveSystem();
$om->updateUserSystemOverrideAccounts();

echo "INFO: Configured SMTP settings to use ${MAILTRAP_HOST}:${MAILTRAP_PORT} ...".PHP_EOL;
