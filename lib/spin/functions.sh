usage() {
    echo "$__usage"

    [ -n "$1" ] && echo "$*" 1>&2

    exit 1
}

config_override() {
    [[ -r .env ]] && source .env
}

detect_os() {
    if [ "$(uname)" == "Darwin" ]; then
        hostOS="osx"
    elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
        hostOS="linux"
    elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
        hostOS="win32"
    elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW64_NT" ]; then
        hostOS="win64"
    fi
}

setup_resolver() {
    case $hostOS in
        osx)
            sudo mkdir -p /etc/resolver
            echo "nameserver 127.0.0.1    # Docker-${DOMAIN}" | sudo tee /etc/resolver/${DOMAIN}
        ;;
        linux)
            # This is only tested on Ubuntu buster/sid
            echo "
INFO: Ensure the ${DOMAIN}-dns container is running.
WARNING: This change is temporary and will revert when your DHCP connection
WARNING: resets, in which case you'll need to run this command again.
"
            echo "
nameserver 127.0.0.1    # Docker-${DOMAIN}
nameserver 127.0.0.53
search lan
" | sudo tee /etc/resolv.conf > /dev/null
        ;;
        win32 | win64)
            echo "ERROR: Not implemented yet."
        ;;
        *)
        ;;
    esac
}

status_check() {
    local dockerStatus=$(docker info > /dev/null 2>&1);

    [[ $? -ne 0 ]] && usage "ERROR: docker is not running."
}

read_stack_env() {
    if [ ! -r "${STACK_FILE}" ]; then
        usage "ERROR: Stack file ${STACK_FILE} does not exist or is not readable."
    else
        source ${STACK_FILE}
    fi

    STACK_DATA="${DATA_LOCATION}/${STACK}"
}

run_builder() {
    echo "INFO: Launching the builder container to run: ${*}"

    docker run -it --rm \
        -v "$(pwd)/stacks:/stacks" \
        -v "$(pwd)/data:/data" \
        -v "${INSTALLER_LOCATION}:/installers" \
        --env-file .env --env-file ${STACK_FILE} \
        -e DOMAIN=${DOMAIN} -e STACK_DATA=${STACK_DATA} \
        -e CONTAINER_DNS=${CONTAINER_DNS} -e CONTAINER_GELF=${CONTAINER_GELF} \
        $(docker build -q images/builder) \
        $*
}

# Create a docker-compose template
create_template() {
    echo "INFO: Creating the stack template ..."
    run_builder /scripts/create_template.sh
}

# Handle various source types
source_type_local() {
    echo "INFO: Preparing local codebase ..."
    run_builder /scripts/handle_local_source.sh
}

source_type_install() {
    echo "INFO: Preparing to install from source ..."
    run_builder /scripts/handle_install_source.sh
}

setup_data() {
    echo "INFO: Creating data folders in: ${STACK_DATA} ..."
    mkdir -p ${STACK_DATA}

    case ${SOURCE_TYPE} in
        local)
            source_type_local
        ;;
        install)
            local sourceZip="${INSTALLER_LOCATION}/${SOURCE}"

            if [ -e "${STACK_DATA}/html/cache/api/metadata/connectors.php" ]; then
                echo "INFO: Looks like the instance is already setup, skipping zip install ..."

                source_type_local
            else
                [ ! -e ${sourceZip} ] && usage "Unable to find ${sourceZip}."

                source_type_install
            fi
        ;;
    esac
}

destroy_data() {
    echo "INFO: Removing persistent data volumes for ${STACK} ..."
    docker volume rm ${STACK}_mysql_data
    docker volume rm ${STACK}_es_data
    docker volume rm ${STACK}_redis_data
    [ -d "${STACK_DATA}" ] && rm -rf ${STACK_DATA}
}

get_container_ip() {
    containerIP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $1)
}

# Spin the stack up
stack_up() {
    local CONTAINERS="main"

    echo "INFO: Launching stack: ${NAME} ..."

    docker-compose -p ${STACK} --project-directory=. \
        -f stacks/${NAME}.yaml up --build -d ${CONTAINERS}
}

# Bring the stack down
stack_down() {
    if [ ! -r "stacks/${NAME}.yaml" ]; then
        echo "WARNING: YAML file missing, re-creating ..."
        create_template
    fi

    docker-compose -p ${STACK} --project-directory=. \
        -f stacks/${NAME}.yaml down
}

# Force a rebuild of the docker images
stack_rebuild() {
    echo "INFO: Rebuilding docker images for stack: ${NAME} ..."
    docker-compose -p ${STACK} --project-directory=. -f stacks/${NAME}.yaml build
}

container_running() {
    containerId=$(docker ps -ql -f "name=${1}")

    return $?
}

# Execute a command on a container (default: /bin/sh)
stack_shell() {
    local commandLine="sh"
    local containerUser="root"
    local containerName=${1:-web}
    local mode="-it"
    shift

    if [ "${containerName}" == "web" ]; then
        containerUser="sugar"
    fi

    if [ -n "${1}" ]; then
        commandLine="${*}"
        mode="-i"
    fi

    if container_running "${STACK}-${containerName}"; then
        docker exec ${mode} --user ${containerUser} ${containerId} ${commandLine}
    else
        echo "ERROR: The ${STACK}-${containerName} container does not appear to be running"
    fi
}

database_backup() {
    mkdir -p ${STACK_DATA}/backups
    local databaseFile=${STACK_DATA}/backups/$(date +"%Y-%m-%d").sql

    if [ -f $databaseFile ]; then
        read -p "A backup file from today already exists, overwrite? " -n 1 -r
        echo
        if [[ ! $REPLY =~ ^[Yy]$ ]]
        then
            return 0
        fi
    fi

    echo "INFO: Dumping the ${STACK}:sugarcrm database into $databaseFile ..."
    stack_shell mysql mysqldump --password=root --order-by-primary --single-transaction -Q --opt --skip-extended-insert sugarcrm > $databaseFile
}

database_restore() {
    if [ ! -f "$1" ]; then
        usage "ERROR: Unable to read $1, does the file exist?"
    fi

    echo "INFO: Restoring $1 into the ${STACK}:sugarcrm database ..."
    cat $1 | stack_shell mysql mysql --password=root sugarcrm
}
