#!/usr/bin/env bash

FOLDER="/var/www/html"
CRON="cron.php"

while true; do
    if [[ -d "${FOLDER}" && -n "${FOLDER}/${CRON}" ]]; then
        cd ${FOLDER}
        echo "Sugar Cron Start"
        php -f ${CRON}
        echo "Sugar Cron End"
    fi

    sleep 60
done
