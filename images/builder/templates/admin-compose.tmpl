version: '3'

volumes:
    seq_data:
        driver: local

services:
    main:
        container_name: "${DOMAIN}-proxy"
        image: "proxy:${DOMAIN}"
        build:
            context: "images/nginx-proxy"
        environment:
            - "HTTPS_METHOD=noredirect"
        ports:
            - "80:80"
        volumes:
            - /var/run/docker.sock:/tmp/docker.sock:ro
        depends_on:
            - mail
            - dns
            - gelf
        networks:
            - ${DOMAIN}-net

    mail:
        container_name: "${DOMAIN}-mail"
        image: mailhog/mailhog
        environment:
            - "VIRTUAL_HOST=mail.${STACK}.${DOMAIN}"
            - "VIRTUAL_PORT=8025"
            - "VIRTUAL_PROTO=http"
        networks:
            - ${DOMAIN}-net

    logs:
        container_name: "${DOMAIN}-logs"
        image: datalust/seq:latest
        volumes:
            - seq_data:/data
        environment:
            - "VIRTUAL_HOST=logs.${STACK}.${DOMAIN}"
            - "ACCEPT_EULA=Y"
        networks:
            - ${DOMAIN}-net

    gelf:
        container_name: "${DOMAIN}-gelf"
        image: datalust/sqelf:latest
        environment:
            - "SEQ_ADDRESS=http://${DOMAIN}-logs:5341"
        ports:
            - "12201:12201/udp"
        networks:
            - ${DOMAIN}-net
        depends_on:
            - logs

    dns:
        container_name: "${DOMAIN}-dns"
        image: "dns:${DOMAIN}"
        build:
            context: "images/dnsmasq"
            args:
                - DOMAIN=${DOMAIN}
        ports:
            - "127.0.0.1:53:53/tcp"
            - "127.0.0.1:53:53/udp"
        networks:
            - ${DOMAIN}-net

networks:
    ${DOMAIN}-net:
        driver: bridge
