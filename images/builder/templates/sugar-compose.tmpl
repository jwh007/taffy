version: '3'

volumes:
    mysql_data:
        driver: local
    es_data:
        driver: local
    redis_data:
        driver: local

services:
    main:
        container_name: "${STACK}-web"
        image: "web:${STACK}"
        build:
            context: "${BUILD_WEB}"
            args:
                - COMPOSER_VERSION
        restart: unless-stopped
        logging:
            driver: "gelf"
            options:
                gelf-address: "udp://${CONTAINER_GELF}:12201"
                tag: "web-logs"
        environment:
            - "APACHE_RUN_USER=sugar"
            - "APACHE_RUN_GROUP=sugar"
            - "VIRTUAL_HOST=web.${STACK}.${DOMAIN}"
            - "MAILTRAP_HOST=${DOMAIN}-mail"
            - "MAILTRAP_PORT=1025"
        dns:
            - ${CONTAINER_DNS}
        volumes:
            - "${STACK_DATA}:/var/www"
            - "./lib/sugar:/home/sugar/tools"
        depends_on:
            - mysql
            - elasticsearch
            - redis
        links:
            - mysql
            - elasticsearch
            - redis
        networks:
            - admin_${DOMAIN}-net
            - internal-net

    cron:
        container_name: "${STACK}-cron"
        image: "cron:${STACK}"
        build:
            context: "${BUILD_CRON}"
        restart: unless-stopped
        logging:
            driver: "gelf"
            options:
                gelf-address: "udp://${CONTAINER_GELF}:12201"
                tag: "cron-logs"
        volumes:
            - "${STACK_DATA}:/var/www"
        depends_on:
            - mysql
            - elasticsearch
            - redis
        links:
            - mysql
            - elasticsearch
            - redis
        networks:
            - admin_${DOMAIN}-net
            - internal-net

    mysql:
        container_name: "${STACK}-mysql"
        image: "mysql:${STACK}"
        build:
            context: "${BUILD_MYSQL}"
        restart: unless-stopped
        volumes:
            - mysql_data:/var/lib/mysql
        ${MYSQL_PORT}
        environment:
            - MYSQL_ROOT_PASSWORD=root
            - MYSQL_USER=sugar
            - MYSQL_PASSWORD=sugar
            - MYSQL_DATABASE=sugarcrm
        networks:
            - internal-net

    elasticsearch:
        container_name: "${STACK}-elasticsearch"
        image: "elasticsearch:${STACK}"
        build:
            context: "${BUILD_ELASTIC}"
        restart: unless-stopped
        environment:
            - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
            - "discovery.type=single-node"
            - "xpack.security.enabled=false"
        volumes:
            - es_data:/usr/share/elasticsearch/data
        ${ELASTIC_PORT}
        ulimits:
            memlock:
                soft: -1
                hard: -1
        networks:
            - internal-net

    redis:
        container_name: "${STACK}-redis"
        image: "${IMAGE_REDIS}"
        restart: unless-stopped
        volumes:
            - redis_data:/data
        networks:
            - internal-net

networks:
    internal-net:

    admin_${DOMAIN}-net:
        external: true
