#!/usr/bin/env bash

unpack_zip() {
    local sourceZip="/installers/${1}"

    if [ -e "$sourceZip" ]; then
        echo "INFO: Unpacking ${sourceZip} ..."
        local sugarFolder=$(unzip -Z1 ${sourceZip} | head -1)
        mkdir -p ${STACK_DATA}/install && \
            unzip -qo ${sourceZip} -d ${STACK_DATA}/install && \
            rsync -qrtvu ${STACK_DATA}/install/${sugarFolder}/ ${STACK_DATA}/html && \
            rm -rf ${STACK_DATA}/install
    fi
}

fix_permissions() {
    cd $1
    bash /scripts/fix_permissions.sh
}

unpack_zip ${SOURCE}

# Hack to handle dollar signs in PHP code that aren't environment variables
export DOLLAR='$'
envsubst < /templates/sugar/config_si.tmpl > ${STACK_DATA}/html/config_si.php
cp /templates/sugar/config_override.php ${STACK_DATA}/html
fix_permissions ${STACK_DATA}/html
