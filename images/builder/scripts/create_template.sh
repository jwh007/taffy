#!/usr/bin/env bash

TEMPLATE_DIR=/templates
STACK_DIR=/stacks

if [ -n "${PORT_MYSQL}" ]; then
    MYSQL_PORT="ports:
            - ${PORT_MYSQL}"
    export MYSQL_PORT
fi

if [ -n "${PORT_ELASTIC}" ]; then
    ELASTIC_PORT="ports:
            - ${PORT_ELASTIC}"
    export ELASTIC_PORT
fi

if [ ! -n "${TEMPLATE}" ]; then
    TEMPLATE="sugar-compose.tmpl"
fi

envsubst < ${TEMPLATE_DIR}/${TEMPLATE} > ${STACK_DIR}/${NAME}.yaml
