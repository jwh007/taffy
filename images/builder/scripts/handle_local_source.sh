#!/usr/bin/env bash

fix_permissions() {
    cd $1
    bash /scripts/fix_permissions.sh
}

if [ ! -n ${STACK_DATA}/html/config_override.php ]; then
    cp /templates/sugar/config_override.php ${STACK_DATA}/html
fi

fix_permissions ${STACK_DATA}/html
