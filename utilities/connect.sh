#!/usr/bin/env bash

if [[ -n "$1" ]]; then
    HOST="$1"
    shift
else
    HOST="cron"
fi

if [[ -n "$1" ]]; then
    USER="$1"
    shift
else
    USER="sugar"
fi

if [[ -n "$@" ]]; then
    COMMAND="$@"
else
    COMMAND="/bin/bash"
fi

docker exec -ti --user ${USER} $(docker ps -ql -f "name=${HOST}") ${COMMAND}
