#!/usr/bin/env bash

echo 'y' | docker volume prune
echo 'y' | docker system prune -a
