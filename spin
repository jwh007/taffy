#!/usr/bin/env bash

source $(dirname $0)/lib/spin/functions.sh

# Override these in the .env file
DOMAIN=local
INSTALLER_LOCATION=${HOME}/Downloads/
LICENSE_KEY=10011001011001011100100011101001
DATA_LOCATION=./data

# Override this is the .env file, or the stack.env
COMPOSER_VERSION=1.9.1

export DOMAIN COMPOSER_VERSION INSTALLER_LOCATION LICENSE_KEY DATA_LOCATION

__usage="Usage: $(basename $0) <stack name> <command>

Stack Name:
  Should match a file in the stacks/ directory.

  Commands:

    up              Create the stack and bring it online
    prepare         Creates the data directories and compose template,
                    but does not start the containers
    down            Stop the containers in a given stack
    rebuild         Rebuild the docker images
    destroy         Remove all persistent volumes (mysql, elastic, etc)
    setupdns        Try to configure the host resolver for OSX/Linux/Windows
    shell           Connect to a container and run a shell

  Database Commands:

    backup          Will create a dump of your mysql database into
                    data/<stack>/backups/$(date +"%Y-%m-%d").sql
    restore <file>  Will restore a database dump, must pass in a filename.
                    The dump must be uncompressed.

  Shell Commands:

    $(basename $0) <stack> shell [service] [command...]

    When running the shell command you can specify which service to connect
    to (default: web), as well as a command to run (default: /bin/sh).

  Examples:

    $(basename $0) example up
    $(basename $0) example shell
    $(basename $0) example shell cron

    $(basename $0) example shell web php /var/www/tools/repair.php
"

config_override
status_check

if container_running ${DOMAIN}-dns; then
    get_container_ip ${DOMAIN}-dns
    CONTAINER_DNS=$containerIP
fi

# GELF Logging uses DNS resolution, and fails against our local DNS server
# without every trying the internal docker DNS, so let's log directly to the IP
if container_running ${DOMAIN}-gelf; then
    get_container_ip ${DOMAIN}-gelf
    CONTAINER_GELF=$containerIP
fi

if [[ -n "$1" ]]; then
    STACK_FILE="stacks/${1}.env"
    shift
fi

if [[ -n "$1" ]]; then
    COMMAND="$1"
    shift
fi

if [ -n "${COMMAND}" ]; then
    case "${COMMAND}" in
        -\?|--help)
            usage
        ;;
        up)
            read_stack_env
            setup_data
            create_template
            stack_up
        ;;
        prepare)
            read_stack_env
            setup_data
            create_template
        ;;
        down)
            read_stack_env
            stack_down
        ;;
        rebuild)
            read_stack_env
            create_template
            # read -p "Delete existing persistent volume data as well? " -n 1 -r
            # echo
            # if [[ $REPLY =~ ^[Yy]$ ]]
            # then
            #     destroy_data
            # fi
            stack_rebuild
        ;;
        destroy)
            read_stack_env
            stack_down
            read -p "Are you sure? This will delete all persistent volume data: " -n 1 -r
            echo
            if [[ $REPLY =~ ^[Yy]$ ]]
            then
                destroy_data
            fi
        ;;
        shell)
            read_stack_env
            stack_shell $*
        ;;
        backup)
            read_stack_env
            database_backup
        ;;
        restore)
            read_stack_env
            database_restore $1
        ;;
        setupdns)
            detect_os
            setup_resolver
        ;;
        *)
            if [ -n "${COMMAND}" ]
            then
                usage "ERROR: Command '${COMMAND}' not recognized"
            fi
        ;;
    esac
else
    usage
fi
